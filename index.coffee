require('cson-config').load('./config.cson')
util  = require 'util'
express = require 'express'

config = process.config
mongoFactory = new (require './libs/factories/mongo-collections') config
app = express()


app.configure ->
	app.use (req, res, next)->
		if req.method.substring(0, 4) is '/api'
			app.use(express.urlencoded())
			app.use(express.json())
			req.headers['content-type'] = 'application/json'
			next()

	app.use (req, res, next) ->
		util.log "#{req.method} #{req.path}"
		req.headers['content-type'] = 'application/json'
		next()


	app.all '*', (req, res, next)->
		res.header 'Access-Control-Allow-Origin', '*'
		res.header 'Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE'
		res.header 'Access-Control-Allow-Headers', 'Content-Type'
		next()

processResponse = (res) ->
	(err, data) ->
		if err
			res.json {error: err}
		else 
			res.json data

# API routs
(require './routes/images') app, config, mongoFactory, processResponse
(require './routes/users') app, config, mongoFactory, processResponse
(require './routes/tags') app, config, mongoFactory, processResponse
	

app.listen config.server.port
console.log "Listening on #{config.server.port}"
