class Writer 
	constructor: (@bson, @users) ->

	createNewUser: (data, done) ->
		@users.insert data, (err, res) ->
			return done err if err
			return done 'user was not created' if not res[0] and res[0]._id
			done null, id: res[0]._id

	updateUser: (data, done) ->
		userId = new @bson.ObjectID data.id
		delete data.id
		@users.update {_id: userId}, {$set: data}, (err, res) ->
			return done err if err
			done null, userId: 'updated'

	deleteUser: (userId, done) ->
		@users.remove {_id: new @bson.ObjectID userId}, (err, res) ->
			return done err if err
			done null, userId: 'deleted'

	invalidToken: (token, done) ->
		@users.update {token: token}, {$unset: {token: 1}}, (err, res) ->
			return done err if err
			done null, {logout: 'done'}

	setNewToken: (userId, token, done) ->
		id = new @bson.ObjectID userId
		@users.update {_id: id}, {$set: {token: token}}, @_doneWithSuccess done

	_doneWithSuccess: (done)->
		(err, res) ->
			return done err if err
			done null, null


module.exports = Writer