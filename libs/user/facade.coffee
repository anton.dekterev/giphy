class Facade
	constructor: (@usersWriter, @usersReader, @auth, @validator) ->

	getUserDetail: (data, done) -> @_tryDo done, =>
		@validator.validUserId data
		@usersReader.getUser data.id, done

	getUserByToken: (data, done) -> @_tryDo done, =>
		@validator.validUserToken data
		@usersWriter.getUserByToken data.token

	getUsers: (data, done) -> @_tryDo done, =>
		data = @_canonicalizeUsersList data
		@validator.validUsers data
		@usersReader.getUsers data.filter, data.sort, data.from, data.size, done
		
	loginByCreditails: (data, done) -> @_tryDo done, =>
		@validator.validCredtails data
		@auth.authByCreditails data.email, data.pass, done

	loginByToken: (data, done) -> @_tryDo done, =>
		@validator.validUserToken data
		@auth.authByToken data.token, done

	updateUser: (data, done) -> @_tryDo done, =>
		@validator.validNewUserData data
		if data._id or data.id
			data.id ?= data._id
			delete data._id
			@usersWriter.updateUser data, done
		else
			@usersWriter.createNewUser data, done

	deleteUser: (data, done) -> @_tryDo done, =>
		@validator.validUserId data
		@usersWriter.deleteUser data.id, done

	logout: (data, done) -> @_tryDo done, =>
		@validator.validUserToken data
		@auth.logout data.token, done

	_tryDo: (onError, tryDo) ->
		try
			tryDo()
		catch e
			console.log e
			onError e

	_canonicalizeUsersList: (data) -> data

module.exports = Facade