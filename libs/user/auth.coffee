class Auth
	constructor: (@reader, @writer, @token) ->

	authByToken: (token, done) ->
		@reader.getUserByToken token, (err, res) ->
			return done err if err
			return done 'Cannot authenticate user' if not res
			done null, res

	authByCreditails: (email, pass, done) ->
		writer = @writer
		tokenFactory = @token
		@reader.getUserByCreditail email, pass, (err, res) ->
			return done err if err
			return done 'user not found' if not res
			token = tokenFactory.createToken()
			userId = new String res._id
			writer.setNewToken userId, token, (err, res) ->
				return done err if err
				done null, token: token

	logout: (token, done) ->
		@writer.invalidToken token, done

module.exports = Auth