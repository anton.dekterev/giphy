class Reader 
	constructor: (@bson, @users) ->

	getUser: (userId, done) ->
		@_findOne {_id: new @bson.ObjectID userId}, done

	getUserByToken: (token, done) ->
		@_findOne {token: token}, done

	getUserByCreditail: (email, pass, done) ->
		@_findOne {email: email, pass: pass}, done

	getUsers: (where, sort, from, size, done) ->
		cursor = @users.find(where).sort(sort)
		cursor.count (err, count) ->
			return done err if err
			return done 'no users fount' if count < from
			cursor.skip(size).limit(size).toArray (err, res) ->
				return done err if err
				done null, {hits: count, data: res}

	_findOne: (query, done) ->
		@users.findOne query, (err, res) ->
			return done err if err
			done null, res

module.exports = Reader