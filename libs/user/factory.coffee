Reader = require './reader'
Writer = require './writer'
Auth = require './auth'
Validator = require './validator'
Facade = require './facade'
Token = require './token'
nodeUuid = require 'node-uuid'

class Factory
	constructor: (@config, @mongoFactory) ->
		@usersCollection = @mongoFactory.createUsersCollection()
		@bson = @mongoFactory.getBSON()

	createUsersFacade: ->
		@facade ?= new Facade(@createWriter(), @createReader(), @createAuth(), @createValidator())

	createWriter: ->
		@writer ?= new Writer(@bson, @usersCollection)

	createReader: ->
		@reader ?= new Reader(@bson, @usersCollection)

	createAuth: ->
		@auth ?= new Auth(@createReader(), @createWriter(), @createToken())

	createValidator: ->
		@validator ?= new Validator()

	createToken: ->
		@token ?= new Token(nodeUuid)

module.exports = Factory