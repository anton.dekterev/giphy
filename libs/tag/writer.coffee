class Writer
	constructor: (@tags, @images) ->

	createTag: (name, urlKey, done) -> 
		@_tagsByUrlKey urlKey, (err) =>
			return done err if err
			@tags.insert {name: name, url_key: urlKey}, (err, res) ->
				return done err if err
				data = res[0]
				done null, {url_key: data.url_key, name: data.name}

	removeTag: (urlKey, done) ->
		@_imagesByUrlKey urlKey, @_removeTag, done

	_tagsByUrlKey: (urlKey, done) =>
		@tags.findOne url_key: urlKey, (err, res) ->
			return done err if err
			return done "Tag #{urlKey} is allready exists" if res
			done null

	_imagesByUrlKey: (urlKey, next, done) ->
		@images.findOne tags: urlKey, (err, res) ->
			return done err if err
			return done 'There are still tagged images with that tag' if res
			next urlKey, done

	_removeTag: (urlKey, done) =>
		@tags.remove {url_key: urlKey}, (err, res) ->
			return done err if err
			data = {}; data[urlKey] = 'removed'
			done null, data

module.exports = Writer