Iconv  = require('iconv').Iconv
iconv = new Iconv 'utf-8',"ASCII//IGNORE//TRANSLIT"

urlize = (input) ->
	iconv.convert(input).toString("utf-8").replace(/\'/g,'').toLowerCase().replace(/[^a-z0-9]+/gi,'-')

class Facade
	constructor: (@mongoReader, @mongoWriter, @validator) ->

	getUserUsedTags: (data, done) -> @_tryDo done, =>
		@validator.validUserId data
		@mongoReader.getUserUsedTags data.userId, done

	getTags: (done) ->
		@mongoReader.getTags done

	createTag: (data, done) -> @_tryDo done, =>
		@validator.validTagName data
		@validator.validDbUserId data
		urlKey = urlize data.name.toString()
		@mongoWriter.createTag data.name, urlKey, done

	removeTag: (data, done) -> @_tryDo done, =>
		@validator.validUrlKey data
		@mongoWriter.removeTag data.urlKey, done

	_tryDo: (onError, tryDo) ->
		try
			tryDo()
		catch e
			console.log e
			onError e
module.exports = Facade