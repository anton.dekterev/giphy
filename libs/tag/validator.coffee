class Validator
	validUserId: (data) -> 
		throw 'variable userId is not set' if not data.userId
	validDbUserId: (data) ->
		throw 'variable user_id is not set' if not data.user_id
	validTagName: (data) ->
		throw 'tag name is not set' if not data.name
	validUrlKey: (data) ->
		throw 'tag urlKey is not set' if not data.urlKey

module.exports = Validator