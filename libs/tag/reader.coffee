class Reader
	constructor: (@tags, @images, @bson) ->

	getUserUsedTags: (userId, done) ->
		@images.distinct 'tags', {created_by: userId}, (err, res) =>
			done err if err
			done null, [] if res.length == 0

			@_getTagsWhere {'url_key': {'$in': res}}, done
	
	getTags: (done) =>
		@_getTagsWhere {}, done

	getImageTags: (imageUrlKey, done) ->
		@images.findOne {url_key: imageUrlKey}, {tags: 1}, (err, res) =>
			@_getTagsWhere {'url_key': {'$in': res.tags}}, done

	_getTagsWhere: (where, done) ->
		@tags.find(where, {_id: 0}).toArray done
		
module.exports = Reader