Reader = require './reader'
Writer = require './writer'
Validator = require './validator'
Facade = require './facade'

class Factory
	constructor: (@config, @mongoFactory) ->
		@imagesCollection = @mongoFactory.createImagesCollection()
		@tagsCollection = @mongoFactory.createTagsCollection()
		@bson = @mongoFactory.getBSON()

	createWriter: ->
		@writer ?= new Writer @tagsCollection, @imagesCollection

	createReader: ->
		@reader ?= new Reader @tagsCollection, @imagesCollection, @bson

	createFacade: ->
		@facade ?= new Facade @createReader(), @createWriter(), @createValidator()

	createValidator: ->
		@validator ?= new Validator()

module.exports = Factory