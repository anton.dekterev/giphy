class Searcher
	constructor: (@esClient)->
		@index = 'images'
		@type = @index

	searchByPhrase: (phrase, from, size, sort, done) ->
		@esClient.search(@index, @type, @contextQuery(phrase, from, size, sort))
			.on 'data', (data)->
				data = JSON.parse data
				return done data if not data.hits
				done null, data.hits
			.on 'error', (err)->
				done err
			.exec()

	contextQuery: (query, from, size)->
		query: 
			query_string:
				fields : ['context.*']
				query: query
		size: size
		from: from

module.exports = Searcher