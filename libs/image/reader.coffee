class Reader
	constructor: (@bson, @imagesCollection)->

	get: (imageId, done)->
		imageId = new @bson.ObjectID imageId
		@imagesCollection.findOne {_id: imageId}, (err, res)->
			done err if err
			done null, res

	list: (tags, from, size, sort, done)->
		sortMongo = {}
		sortMongo[sort?.sortBy || 'creation_time'] = sort?.sortDir || -1
		where = if tags then {tags: tags} else {}
		@_list where, from, size, sortMongo, done


	fetchByIds: (ids, done) ->
		ids = (new @bson.ObjectID imageId for imageId in ids)
		@imagesCollection.find({_id: {$in: ids}}).toArray (err, res) ->
			return done err if err
			done null, res

	_list: (where, from, size, sort, done) ->
		cursor = @imagesCollection.find(where).sort(sort)
		cursor.count (err,count)->
			done err if err
			cursor.skip(from).limit(size).toArray()
				.done (docs)->
					done null, {hits: count, data: docs}
				.fail done

module.exports = Reader
