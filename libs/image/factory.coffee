Reader = require './reader'
Writer = require './writer'
Searcher = require './searcher'
Validator = require './validator'
Facade = require './facade'
TagFactory = require '../tag/factory'
ElasticSearchClient = require 'elasticsearchclient'
EsSynchronizer = require './es-synchronizer'

class Factory
	constructor: (@config, @mongoFactory) ->
		@imagesCollection = @mongoFactory.createImagesCollection()
		@tagFactory = new TagFactory @config, @mongoFactory
		@bson = @mongoFactory.getBSON()

	createImageFacade: ->
		@facade ?= new Facade @createEsSynchronizer(), @createReader(), @createSearcher(), @tagFactory.createReader(), @createValidator()

	createWriter: ->
		@writer ?= new Writer @bson, @imagesCollection

	createReader: ->
		@reader ?= new Reader @bson, @imagesCollection

	createSearcher: ->
		@searcher ?= new Searcher @createEsClient()

	createValidator: ->
		@validator ?= new Validator()

	createEsClient: ->
		@esClient ?= new ElasticSearchClient({
			host: @config.elasticsearch.host
			port: @config.elasticsearch.port
		})

	createEsSynchronizer: ->
		@esSynchronizer ?= new EsSynchronizer(@createWriter(), @createEsClient())
			

module.exports = Factory