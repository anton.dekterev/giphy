class Writer

	constructor: (@bson, @imagesCollection) ->

	remove: (imageId, done) ->
		imageId = new @bson.ObjectID imageId
		@imagesCollection.remove {_id: imageId}, (err, res)->
			return done err if err
			done null, {deleted: true}

	detachTags: (urlKey, tags, done) ->
		@imagesCollection.update {url_key: urlKey}, {$pullAll: {tags: tags}}, done

	attachTags: (urlKey, tags, done) ->
		@imagesCollection.update {url_key: urlKey}, {$addToSet: {tags: {$each: tags}}}, done

	processUpdate: (data, done) ->
		id = new @bson.ObjectID data._id
		delete data._id

		@imagesCollection.update {_id:  id}, {$set: data}, (err, res) ->
			return done err if err
			done null, {id: id, updated: true}

	processInsert: (data, done)->
		@imagesCollection.insert data, (err, res) ->
			return done err if err
			done null, {id: res[0]._id, updated: true}

module.exports = Writer