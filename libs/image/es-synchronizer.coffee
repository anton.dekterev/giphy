shortId = require 'short-mongo-id'

class EsSynchronizer

	constructor: (@writer, @esClient) ->
		@index = 'images'
		@type = 'images'

	processUpdate: (data, done) ->
		@writer.processUpdate data, (err, res) =>
			done err if err
			data.id = res.id
			content = @_fetchEsData data
			@_processUpdate content, ->
				done null, res

	processInsert: (data, done) ->
		@writer.processInsert data, (err, res) =>
			done err if err
			data.id = res.id
			content = @_fetchEsData data
			@_processInsert content, (err ,esRes) ->
				done null, res

	remove: (imageId, done) ->
		@writer.remove imageId, (err, res) =>
			done err if err
			@_processRemove imageId, ->
				done null, res

	_processUpdate: (data, done) =>
		imageId = shortId(data.image_id)
		data = doc: data, doc_as_upsert: true
		@esClient.update(@index, @type, imageId, data)
			.on 'data', (data) ->
				done null, data
			.on 'error', (err) ->
				done err
			.exec()

	_processInsert: (data, done) =>
		@esClient.index(@index, @type, data, shortId(data.image_id))
			.on 'data', (data) ->
				done null, data
			.on 'error', (err) ->
				done err
			.exec()

	attachTags: (urlKey, tags, done) ->
		@writer.attachTags urlKey, tags, done

	detachTags: (imageId, tags, done) ->
		@writer.detachTags imageId, tags, done

	_fetchEsData: (data) ->
		content = @_fetchContent data

		image_id: data.id
		tags: data.tags
		context:
			title: content.title
			description: content.description
			content: content.content

	_processRemove: (imageId, done) =>
		@esClient.deleteDocument(@index, @type, shortId(imageId))
			.on 'data', (data) ->
				done null, data
			.on 'error', (err)->
				done err
			.exec()

	_fetchContent: (data) ->
		res = []
		for lang, content of data.context
			content[lang] = lang
			res.push content
		res.pop()

module.exports = EsSynchronizer