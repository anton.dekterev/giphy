class Facade
	constructor: (@mongoWriter, @mongoReader, @esSearcher, @tagsReader, @validator) ->

	put: (data, done) -> @_tryDo done, =>
		@validator.validContent data
		data = @_canonicalizeContext data
		if data._id or data.id
			(data._id = data.id; delete data.id) if data.id
			@mongoWriter.processUpdate data, done
		else
			@validator.validCreatedBy data
			data = @_canonicalizeCreatedTime data
			@mongoWriter.processInsert data, done

	get: (data, done) -> @_tryDo done, =>
		@validator.validId data
		@mongoReader.get data.id, (err, data) =>
			return done err if err
			done null, @_contextToContent data

	remove: (data, done) -> @_tryDo done, =>
		@validator.validId data
		@mongoWriter.remove data.id, done

	list: (where, done) ->
		where = @_canonicalizeImagesList where
		@mongoReader.list where.tags, where.from, where.size, where.sort, done

	attachTags: (data, done) -> @_tryDo done, =>
		@validator.validTags data
		@mongoWriter.attachTags data.urlKey, data.tags, (err, res) =>
			return data err if err
			@tagsReader.getImageTags data.urlKey, done

	detachTags: (data, done) -> @_tryDo done, =>
		@validator.validTags data
		@mongoWriter.detachTags data.urlKey, data.tags, (err, res) =>
			return data err if err
			@tagsReader.getImageTags data.urlKey, done

	search: (data, done) -> @_tryDo done, =>
		@validator.validateSearchData data
		data = @_canonicalizeSearchData data
		@esSearcher.searchByPhrase data.phrase, data.from, data.size, data.sort, (err, esRes) =>
			return done err if err
			imagesIds = (image._source.image_id for image in esRes.hits)
			@mongoReader.fetchByIds imagesIds, (err, res) =>
				return done err if err
				res = (@_contextToContent data for data in res)
				done null, {
					hits: esRes.total
					data: res
				}

	_tryDo: (onError, tryDo) ->
		try
			tryDo()
		catch e
			console.log e
			onError e

	_canonicalizeImagesList: (data) ->
		data.from ?= 0
		data.size ?= 50
		data

	_canonicalizeCreatedTime: (data) ->
		data.created_time = new Date()
		data

	_canonicalizeContext: (data) ->
		if data.content
			data.context = {}
			data.context[data.lang] = data.content
			delete data.content
			delete data.lang
		data

	_contextToContent: (data) ->
		if data.context
			context = []
			for lang, content of data.context
				context.push {lang: lang, content: content}	
			data.lang = context[0].lang
			data.content = context[0].content
			delete data.context
		data

	_canonicalizeSearchData: (data) ->
		data.from = 0 if not data.from
		data.size = 10 if not data.size
		data

module.exports = Facade