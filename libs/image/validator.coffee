class Validator
	validTags: (data) ->
		throw 'Parameter urlKey is not set' if not data.urlKey
		throw 'Tags is not set' if not data.tags
		throw 'Tags should to be array' if not data.tags instanceof Array

	validCreatedBy: (data) ->
		throw 'created_by is not set' if not data['created_by']

	validId: -> (data) ->
		throw 'id is not set' if not data.id

	validContent: (data) ->
		@validLang data
		throw 'content should be set' if not data.content

	validLang: (data) ->
		throw 'lang should be set' if not data.lang

	validateSearchData: (data) ->
		throw 'phrase is not set' if not data.phrase

module.exports = Validator