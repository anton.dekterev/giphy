mongoq = require 'mongoq'

class Factory
	constructor: (@config) ->
		@db = mongoq(@config.mongo.db.main)

	createImagesCollection: ->
		@imagesCollection = @db.collection 'images'

	createTagsCollection: ->
		@tagsCollection ?= @db.collection 'tags'
	
	createUsersCollection: ->
		@usersCollection ?= @db.collection 'users'

	getBSON: ->
		mongoq.BSON


module.exports = Factory