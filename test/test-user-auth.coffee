request = require 'superagent'
chai = require 'chai'
assert = chai.assert
apiUrl = 'http://localhost:8000/api'


userId = null

callApi = (path, done) ->
	request
			.get(apiUrl + path)
			.end done
token = ''
describe 'users auth', ->

	before (done) ->
		request
			.put(apiUrl + '/users/user')
			.send({
				user_name: 'anton'
				name: 'Anton Dekterov'
				email: 'anton@google.com'
				pass: '1234'
			})
			.end (err, res) ->
				userId = res.body.id
				assert.isString userId
				done()

	it 'should login user with creditails', (done) ->
		request
			.post(apiUrl + '/auth/login')
			.send({email: 'anton@google.com', pass: '1234'})
			.end (err, res) ->
				data = res.body
				assert.isString data.token
				token = data.token
				done()

	it 'should login by token', (done) ->
		request
			.post(apiUrl + '/auth/token')
			.send({token: token})
			.end (err, res) ->
				data = res.body
				assert.isString data.email
				assert.isString data.name
				done()

	it 'should logout by token', (done) ->
		request
			.post(apiUrl + '/auth/logout')
			.send({token: token})
			.end (err, res) ->
				data = res.body
				assert.isTrue data.logout == 'done'
				done()

	after (done) ->
		request
			.del(apiUrl + '/users/user')
			.send({
				id: userId
			})
			.end (err, res) ->
				assert.isTrue not res.body.error
				done()