request = require 'superagent'
validate = require('jsonschema').validate
apiUrl = 'http://localhost:8000/api/images'
chai = require 'chai'
assert = chai.assert

imageId = null

describe 'service image', ->
	before (done)->
		request
			.put(apiUrl + '/image')
			.send(
				url_key: 'new-testing-image'
				lang: 'en'
				content:
					content: 'content of the image. Long content.'
					title: 'New Line Image'
					description: 'description if uploaded image'
				created_by: 'user id'
			)
			.end (err, res) ->
				data = res.body
				assert.isString data.id
				imageId = data.id
				done()

	after (done)->
		request
			.del(apiUrl + '/image')
			.send({
				id: imageId
			})
			.end (err, res) ->
				assert.isTrue res.body.deleted
				done()

	it 'should find data', (done)->
		request
			.post(apiUrl + '/search')
			.send({
				'phrase': 'Long'
				'from': 0
				'size': 10
			})
			.end (err, res)->
				assert.isTrue res.body.hits >= 1
				done()