request = require 'superagent'
apiUrl = 'http://localhost:8000/api'
chai = require 'chai'
assert = chai.assert

imageId = null

describe 'service image', ->
	before (done) ->
		request
			.put(apiUrl + '/images/image')
			.send(
				url_key: 'new-testing-image-111'
				lang: 'en'
				tags: ['actor']
				content:
					content: 'same text'
					title: 'new testing image'
					description: 'description'
				created_by: 'user id'
			)
			.end (err, res) ->
				data = res.body
				assert.isString data.id
				imageId = data.id
				done()

	after (done)->
		request
			.del(apiUrl + '/images/image')
			.send({
				id: imageId
			})
			.end (err, res) ->
				assert.isTrue res.body.deleted
				done()

	it 'should add new tags', (done) ->
		request
			.put(apiUrl + '/images/tags')
			.send(
				urlKey: 'new-testing-image-111'
				tags: ['brands', 'auto']
			)
			.end (err, res) ->
				data = res.body
				assert.isArray data
				done()

	it 'should delete tags', (done) ->
		request
			.del(apiUrl + '/images/tags')
			.send(
				urlKey: 'new-testing-image-111'
				tags: ['brands', 'auto']
			)
			.end (err, res) ->
				data = res.body
				console.log data
				assert.isArray data
				done()