request = require 'superagent'
validate = require('jsonschema').validate
apiUrl = 'http://localhost:8000/api/users'
chai = require 'chai'
assert = chai.assert

userId = null

describe 'users', ->
	it 'should create new user', (done) ->
		request
			.put(apiUrl + '/user')
			.send({
				user_name: 'anton'
				name: 'Anton Dekterov'
				email: 'anton.dekterov@socialbakers.com'
				pass: '1234'
			})
			.end (err, res) ->
				userId = res.body.id
				assert.isString userId
				done()

	it 'should update user', (done) ->
		request
			.put(apiUrl + '/user')
			.send({
				_id: userId
				email: 'anton@google.com'
			})
			.end (err, res) ->
				assert.isTrue not res.body.error
				done()

	it 'should return list of users', (done) ->
		request
			.post(apiUrl)
			.send({
				filter: {email: 'anton@google.com'}
				size: 1
			})
			.end (err, res) ->
				assert.isTrue res.body.data.length == 1
				done()

	it 'should return user detail', (done) ->
		request
			.post(apiUrl + '/user')
			.send({
				id: userId
			})
			.end (err, res) ->
				assert.isTrue not res.body.error
				done()

	it 'should delete user', (done) ->
		request
			.del(apiUrl + '/user')
			.send({
				id: userId
			})
			.end (err, res) ->
				assert.isTrue not res.body.error
				done()