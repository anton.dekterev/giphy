request = require 'superagent'
apiUrl = 'http://localhost:8000/tags'
chai = require 'chai'
assert = chai.assert

tagUrlKey = null
userId = 'user_id'
describe 'tags api', ->

	it 'should insert new image', (done)->
		request
			.put(apiUrl + '/image')
			.send(
				url_key: 'new-testing-image'
				lang: 'en'
				content:
					content: 'same text'
					title: 'new testing image'
					description: 'description'
				created_by: 'user id'
			)
			.end (err, res) ->
				data = res.body
				assert.isString data.id
				imageId = data.id
				done()

	it 'should respose err message', (done) ->
		request
			.put(apiUrl + '/tag')
			.send()
			.end (err, res) ->
				data = res.body
				assert.isString data.error
				tagUrlKey = data.url_key
				done()

	it 'should create tag', (done) ->
		request
			.put(apiUrl + '/tag')
			.send({name: new Date().getTime(), user_id: userId})
			.end (err, res) ->
				data = res.body
				assert.isString data.url_key
				tagUrlKey = data.url_key
				done()

	it 'should find tags by user id', (done) ->
		request
			.post(apiUrl + '/by-user')
			.send({userId: userId})
			.end (err, res) ->
				data = res.body
				console.log data
				console.log data.error if data.error
				assert.isTrue data instanceof Array and data.length > 0
				done()

	it 'should return list of tags', (done) ->
		request
			.get(apiUrl)
			.end (err, res) ->
				data = res.body
				assert.isTrue data instanceof Array and data.length > 0
				done()

	it 'should delete tag', (done) ->
		request
			.del(apiUrl + '/tag')
			.send({urlKey: tagUrlKey})
			.end (err, res) ->
				data = res.body
				console.log data.error if data.error
				assert.isString data[tagUrlKey]
				done()