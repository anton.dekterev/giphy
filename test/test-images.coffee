request = require 'superagent'
validate = require('jsonschema').validate
apiUrl = 'http://localhost:8000/api/images'
chai = require 'chai'
assert = chai.assert

imageId = null

describe 'service image', ->
	it 'should insert new image', (done)->
		request
			.put(apiUrl + '/image')
			.send(
				url_key: 'new-testing-image'
				lang: 'en'
				content:
					content: 'same text'
					title: 'new testing image'
					description: 'description'
				created_by: 'user id'
			)
			.end (err, res) ->
				data = res.body
				assert.isString data.id
				imageId = data.id
				done()

	it 'should update image`s data', (done) ->
		request
			.put(apiUrl + '/image')
			.send(
				_id: imageId
				lang: 'en'
				content: 
					title: 'new title'
					title_next: 'next title'
					content: 'same text'
					description: 'description'
			)
			.end (err, res)->
				assert.isTrue res.body.updated
				done()

	it 'should give image data', (done) ->
		request
			.post(apiUrl + '/image')
			.send({
				'id': imageId
			})
			.end (err, res) ->
				assert.equal imageId, res.body._id
				done()

	it 'should give list of images', (done)->
		getImagesList (err, res) ->
			schema =
				title: 'images list schema'
				type: 'object'
				properties:
					hits: {type: 'integer'}
					data: {type: 'array'}
			
			errors = validate(res.body, schema).errors
			assert.lengthOf errors, 0
			done()

	it 'should remove image', (done)->
		request
			.del(apiUrl + '/image')
			.send({
				id: imageId
			})
			.end (err, res) ->
				assert.isTrue res.body.deleted
				done()


getImagesList = (done)->
	request
			.post(apiUrl)
			.send({
				'from': 0
				'size': 100
			})
			.end done