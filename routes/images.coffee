ImagesFactory = require '../libs/image/factory'

module.exports = (app, config, mongoFactory) ->
	factory = new ImagesFactory(config, mongoFactory)
	facade = factory.createImageFacade()

	###
		Endpoint for image detail
		Input: {
			id: image_id
		}

		Output: {
			_id: image_id
			name: image_name
			url_key: image_url_key
		}
	###
	app.post '/api/images/image', (req, res, next) ->
		facade.get req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData

	###
		Endpoint create or update image
		If document with id is available in database, record will be updated
		if not mongo will create new one

		Input: {
			id: id
			name: image_name
			url_key: url_key
			whatever you want
		}
		Output: {
			_id: image_id
			name: name
			...
		}
	###
	app.put '/api/images/image', (req, res, next) ->
		facade.put req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData

	###
		Delete image by id

		Input: {
			id: image_id
		}
	###
	app.delete '/api/images/image', (req, res, next) ->
		facade.remove req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData

	###
		Return list of images
		Input: {
			tags: ['tag']
			sort: {
				sortBy: 'name'
				sortDir: 1
			}
			from: 0
			size: 10
		}
	###
	app.post '/api/images', (req, res, next) ->
		facade.list req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData

	###
		
	###
	app.post '/api/images/search', (req, res, next) ->
		facade.search req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData

	###
		Atach tags to image
		Return list of image's tags

		Input: {
			urlKey: image_url_key
			tags: [tag1, tag2]
		}
		Output: [{
			name: tag1,
			url_key: tag1
		}]
	###
	app.put '/api/images/tags', (req, res, next) ->
		facade.attachTags req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData

	###
		Atach tags to image
		Return list of image's tags

		Input: {
			urlKey: image_url_key
			tags: [tag1, tag2]
		}
		Output: [{
			name: tag1,
			url_key: tag1
		}]
	###
	app.delete '/api/images/tags', (req, res, next) ->
		facade.detachTags req.body, (err, resData) ->
			if err then res.json {error: err} else res.json resData