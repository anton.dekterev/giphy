UsersFactory = require '../libs/user/factory'

module.exports = (app, config, mongoFactory, processResponse) ->
	factory = new UsersFactory(config, mongoFactory)
	facade = factory.createUsersFacade()

	###
		Endpoint for user detail
		Input: {
			id: user_id
		}

		Output: {
			_id: user_id
			name: iuser_name
			...
		}
	###
	app.post '/api/users/user', (req, res, next) ->
		facade.getUserDetail req.body, processResponse res

	###
		Endpoint create or update user
		If document with id is available in database, record will be updated
		if not mongo will create new one

		Input: {
			id: id
			name: user_name
			whatever you want
		}
		Output: {
			_id: user_id
			name: name
			...
		}
	###
	app.put '/api/users/user', (req, res, next) ->
		facade.updateUser req.body, processResponse res

	###
		Delete user by id

		Input: {
			id: user_id
		}
	###
	app.delete '/api/users/user', (req, res, next) ->
		facade.deleteUser req.body, processResponse res

	###
		Return list of users
		Input: {
			filter: {
				'name': 'Anton'
			}
			sort: {
				sortBy: 'name'
				sortDir: 1
			}
			from: 0
			size: 10
		}
	###
	app.post '/api/users', (req, res, next) ->
		facade.getUsers req.body, processResponse res

	###
		Authenticate user
		Input: {
			email: email
			pass: pass
		}
	###
	app.post '/auth/login', (req, res, next) ->
		facade.loginByCreditails req.body, processResponse res

	###
		Authenticate user
		Input: {
			token: token
		}
	###
	app.post '/api/auth/logout', (req, res, next) ->
		facade.logout req.body, processResponse res

	###
		Authenticate user
		Input: {
			token: token
		}
		Output: {
			name: name
			email: email
		}
	###
	app.post '/api/auth/token', (req, res, next) ->
		facade.loginByToken req.body, processResponse res