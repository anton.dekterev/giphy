TagsFactory = require '../libs/tag/factory'

module.exports = (app, config, mongoFactory, processResponse) ->
	factory = new TagsFactory(config, mongoFactory)
	facade = factory.createFacade()

	app.get '/api/tags', (req, res, next) ->
		facade.getTags processResponse res

	app.post '/api/tags/by-user', (req, res, next) ->
		facade.getUserUsedTags req.body, processResponse res

	app.put '/api/tags/tag', (req, res, next) ->
		facade.createTag req.body, processResponse res

	app.delete '/api/tags/tag', (req, res, next) ->
		facade.removeTag req.body, processResponse res