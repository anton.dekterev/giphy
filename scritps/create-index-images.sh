curl -XPOST localhost:9200/images -d '{
    "mappings" : {
        "images" : {
            "properties" : {
                "image_id" : { "type" : "string", "index" : "not_analyzed"},
                "url_key" : { "type" : "string", "index" : "not_analyzed"},
                "tags" : { "type" : "string", "index" : "analyzed" },
                "context" : {
                    "properties" : {
                        "title" : { "type" : "string", "index" : "analyzed" },
                        "description" : { "type" : "string", "index" : "analyzed" },
                        "content" : { "type" : "string", "index" : "analyzed" }
                    }
                }
            }
        }
    }
}'