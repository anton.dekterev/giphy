db = connect("localhost:27017/giphy_users");

var data = [
	{ "name" : "simple image", "description" : "simple image description",  "url_key": "simple-image" },
	{ "name" : "other image", "description" : "other image description",  "url_key": "other-image" },
	{ "name" : "new image", "description" : "new image description", "url_key": "new-image" },
	{ "name" : "new image 1", "description" : "new image 1 description", "url_key": "new-image-1" },
	{ "name" : "new image 2", "description" : "new image 2 description", "url_key": "new-image-2" },
	{ "name" : "new image 3", "description" : "new image 3 description", "url_key": "new-image-3" }
]

data.forEach(function(doc){
	db.images.insert(doc)
})